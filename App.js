import React, {Component} from 'react';
import { Platform, StyleSheet, Text, View, FlatList } from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

class App extends React.Component {
  state = {
    refreshing: true,
  }

  componentDidMount() {
    setInterval(() => this.setState({ refreshing: false }), 100)
  }

  onRefresh() {
    console.log("refreshing");
  }

  render() {
    return (
      <View style={{ flex: 1, alignItems: 'stretch' }}>
        <Text>Check your console, if you pull the list, "refreshing" is not logged.</Text>

        <FlatList
          style={{ backgroundColor: 'grey' }}
          refreshing={this.state.refreshing}
          onRefresh={this.onRefresh}
          renderItem={() => {}}
          items={[]}
        />
      </View>
      
    );
  }
}

export default App;